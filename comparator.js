function nodesEquals(left, right, same) {
    same = same ?? true;

    if (!left || !right) {
        return left === right;
    }

    return (
        left.value === right.value &&
        (!same || left.rank === right.rank) &&
        nodesEquals(left.left, right.left, same) &&
        nodesEquals(left.right, right.right, same)
    );
}

class Comparator {
    constructor(left, right) {
        this.left = left;
        this.right = right;
    }

    insert(value) {
        this.left.insert(value);
        this.right.insert(value);
    }

    delete(value) {
        this.left.delete(value);
        this.right.delete(value);
    }

    equals() {
        return nodesEquals(this.left.root, this.right.root);
    }

    similar() {
        return nodesEquals(this.left.root, this.right.root, false);
    }
}
