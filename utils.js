Array.prototype.equals = function (other) {
    if (this.length != other.length) {
        return false;
    }

    let i = this.length;
    while (--i >= 0) {
        if (this[i] !== other[i]) {
            return false;
        }
    }

    return true;
};

class Queue {
    constructor() {
        this.q = [];
    }

    enqueue(x) {
        this.q.push(x);
    }

    dequeue() {
        return this.q.shift();
    }

    isEmpty() {
        return this.q.length == 0;
    }
}
