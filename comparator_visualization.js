let lRecorder = new Recorder(
    d3.select("#left").graphviz(),
    null,
    "left"
).renderAtOnce();
let rRecorder = new Recorder(
    d3.select("#right").graphviz(),
    null,
    "right"
).renderAtOnce();

let left = null;
let right = null;
setTrees(new AVLTree(), new WAVLTree());

function operationCallback(lMethod, rMethod, id) {
    let number = document.getElementById(id).value;
    let value = parseInt(number);
    if (number === "" || isNaN(value)) {
        return false;
    }

    lMethod(value);
    rMethod(value);
    document.getElementById(id).value = "";
    return false;
}

function insertCallback() {
    return operationCallback(
        left.insert.bind(left),
        right.insert.bind(right),
        "insertInput"
    );
}

function deleteCallback() {
    return operationCallback(
        left.delete.bind(left),
        right.delete.bind(right),
        "deleteInput"
    );
}

function setTrees(newLeft, newRight, msg) {
    left = newLeft;
    left.recorder = lRecorder;

    right = newRight;
    right.recorder = rRecorder;

    for (let t of [left, right]) {
        t.recorder.clear();
        t.record(msg);
    }
}

function switchTree(TreeType, side) {
    let [lType, rType] =
        side == "left"
            ? [TreeType, right.constructor]
            : [left.constructor, TreeType];
    setTrees(new lType(), new rType());
}

// #region prepared scenarios
function prepareDifferentRanks() {
    let newLTree = new AVLTree();
    let newRTree = new WAVLTree();

    for (let key of [0, -7, 1, -1]) {
        newLTree.insert(key);
        newRTree.insert(key);
    }

    document.getElementById("lAvlTreeBtn").checked = true;
    document.getElementById("rWavlTreeBtn").checked = true;

    setTrees(newLTree, newRTree, "Prepare tree");

    document.getElementById("deleteInput").value = 1;
}

function prepareDifferentTrees() {
    let newLTree = new AVLTree();
    let newRTree = new WAVLTree();

    for (let key of [0, -31, 2, 3, 4, 1, 6, -1, -4, -3, -2, 31]) {
        newLTree.insert(key);
        newRTree.insert(key);
    }

    document.getElementById("lAvlTreeBtn").checked = true;
    document.getElementById("rWavlTreeBtn").checked = true;

    setTrees(newLTree, newRTree, "Prepare tree");

    document.getElementById("deleteInput").value = -31;
}
// #endregion prepared scenarios

const tooltipTriggerList = document.querySelectorAll(
    '[data-bs-toggle="tooltip"]'
);
const tooltipList = [...tooltipTriggerList].map(
    (tooltipTriggerEl) => new bootstrap.Tooltip(tooltipTriggerEl)
);

async function render() {
    await Promise.all([
        new Promise(() => lRecorder.render()),
        new Promise(() => rRecorder.render()),
    ]);
    setTimeout(render);
}
render();
