let recorder = new Recorder(
    d3.select("#graph").graphviz(),
    document.getElementById("comment"),
    "graph"
);

let tree = new WAVLTree();
tree.recorder = recorder;

function operationCallback(method, id) {
    let number = document.getElementById(id).value;
    let value = parseInt(number);
    if (number === "" || isNaN(value)) {
        return false;
    }

    method(value);
    document.getElementById(id).value = "";
    return false;
}

function insertCallback() {
    return operationCallback(tree.insert.bind(tree), "insertInput");
}

function deleteCallback() {
    return operationCallback(tree.delete.bind(tree), "deleteInput");
}

function switchTree(TreeType) {
    tree = new TreeType();
    tree.recorder = recorder;
    recorder.clear();
    tree.record("");
}

// #region prepared scenarios
function prepareSingleRotationAfterDelete() {
    let newTree = new WAVLTree();

    for (let key of [0, 1, 2, -1]) {
        newTree.insert(key);
    }

    document.getElementById("wavlTreeBtn").checked = true;

    tree = newTree;
    tree.recorder = recorder;
    recorder.clear();
    tree.record("Prepare tree");

    document.getElementById("deleteInput").value = 2;
}

function prepareDoubleRotationAfterDelete() {
    let newTree = new WAVLTree();

    for (let key of [0, -7, 1, -1]) {
        newTree.insert(key);
    }

    document.getElementById("wavlTreeBtn").checked = true;

    tree = newTree;
    tree.recorder = recorder;
    recorder.clear();
    tree.record("Prepare tree");

    document.getElementById("deleteInput").value = 1;
}

function prepareDemotionOfBothNodesDuringDelete() {
    let newTree = new WAVLTree();

    for (let key of [0, 1, 2, 3, -2, -3, -1]) {
        newTree.insert(key);
    }

    for (let key of [-2]) {
        newTree.delete(key);
    }

    document.getElementById("wavlTreeBtn").checked = true;

    tree = newTree;
    tree.recorder = recorder;
    recorder.clear();
    tree.record("Prepare tree");

    document.getElementById("deleteInput").value = 2;
}
// #endregion prepared scenarios

const tooltipTriggerList = document.querySelectorAll(
    '[data-bs-toggle="tooltip"]'
);
const tooltipList = [...tooltipTriggerList].map(
    (tooltipTriggerEl) => new bootstrap.Tooltip(tooltipTriggerEl)
);

function render() {
    recorder.render();
    setTimeout(render);
}
render();
