# WAVL Tree Visualization

## Used dependencies

- `d3`, `d3-graphviz` and `WASM` - used for rendering the DOT files and animating
- `bootstrap` - used for CSS and frontend elements

They are fetched via CDN (Content Delivery Network), so you need internet connection,
unless they are cached (may differ with browsers).

## Running

Simply run an HTTP server from the root of the directory containing web, e.g.

    mfocko@feynman on  main at 16:18:00
    web λ python3 -m http.server
    Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...

and follow the link it gives you :)

## Makefile

Targets to ease the quality of life.

### `run`

Alias to `python3 -m http.server`

### `deploy`

Deploys the page to https://me.mfocko.xyz/wavl/web
