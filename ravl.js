class RAVLTree extends WAVLTree {
    isCorrectNode(node, recursive) {
        if (!node) {
            return true;
        }

        if (!nodeDifferences(node).filter((d) => d <= 0).length > 0) {
            return false;
        }

        return (
            !recursive ||
            (this.isCorrectNode(node.left) && this.isCorrectNode(node.right))
        );
    }

    deleteRebalance(node, parent) {
        // no-op
        this.record("No rebalancing occurs");
        return;
    }
}
