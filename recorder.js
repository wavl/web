const DURATION = 750;

class Record {
    constructor(tree, message) {
        this.tree = tree;
        this.message = message;
    }

    render(graph, textBox) {
        graph.renderDot(this.tree);
        if (textBox) {
            textBox.textContent = this.message;
        }
    }
}

class Recorder {
    constructor(graph, textBox, id) {
        this.graph = graph;
        this.textBox = textBox;
        this.id = id;

        this.atOnce = false;

        this.clear();
        this.initGraph();
    }

    clear() {
        this.rendered = -1;
        this.rendering = false;
        this.states = new Array();
    }

    renderAtOnce() {
        this.atOnce = true;
        return this;
    }

    unblockRendering() {
        this.rendering = false;
    }

    initGraph() {
        this.graph.transition(() =>
            d3
                .transition("main")
                .ease(d3.easeLinear)
                .on("end", this.unblockRendering.bind(this))
                .delay(500)
                .duration(DURATION)
        );
    }

    render() {
        if (this.atOnce) {
            if (this.rendered != this.states.length - 1) {
                this.rendered = this.states.length - 1;
                this.states[this.rendered].render(this.graph, this.textBox);
            }

            return;
        }

        if (
            this.rendering ||
            !this.states ||
            this.rendered == this.states.length - 1
        ) {
            return;
        }

        this.rendering = true;

        this.rendered++;
        this.states[this.rendered].render(this.graph, this.textBox);
    }

    previous() {
        throw "not implemented!";
    }

    next() {
        throw "not implemented!";
    }

    record(tree, message, ...highlightedNodes) {
        // TODO: adjust join if necessary
        this.states.push(
            new Record(tree.toDot(...highlightedNodes).join(""), message)
        );
        this.render();
    }
}
